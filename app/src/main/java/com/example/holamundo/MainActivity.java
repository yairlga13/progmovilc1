package com.example.holamundo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText txtNombre;
    private Button button;
    private Button buttonLimpiar;
    private Button buttonCerrar;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = findViewById(R.id.txtNombre); // Aquí está el cambio
        button = findViewById(R.id.button);
        buttonLimpiar = findViewById(R.id.buttonLimpiar); // Aquí está el cambio
        buttonCerrar = findViewById(R.id.buttonCerrar); // Aquí está el cambio

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString().trim();
                if (!nombre.isEmpty()) {
                    mostrarAlerta("Texto ingresado", "¡Hola, " + nombre + "!");
                } else {
                    mostrarAlerta("Alerta", "Por favor ingresa una palabra.");
                }
            }
        });

        buttonLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNombre.setText(""); // Limpiar el EditText
            }
        });

        buttonCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); // Cerrar la actividad
            }
        });
    }

    private void mostrarAlerta(String titulo, String mensaje) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo)
                .setMessage(mensaje)
                .setPositiveButton("Aceptar", null)
                .show();
    }
}
