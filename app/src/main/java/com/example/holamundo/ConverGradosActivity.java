package com.example.holamundo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ConverGradosActivity extends AppCompatActivity {

    private RadioButton radioFahrenheitToCelsius;
    private RadioButton radioCelsiusToFahrenheit;
    private EditText editTextInput;
    private Button buttonCalculate;
    private Button buttonClear;
    private Button buttonClose;
    private TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conver_grados);

        radioFahrenheitToCelsius = findViewById(R.id.radio_fahrenheit_to_celsius);
        radioCelsiusToFahrenheit = findViewById(R.id.radio_celsius_to_fahrenheit);
        editTextInput = findViewById(R.id.edit_text_input);
        buttonCalculate = findViewById(R.id.button_calculate);
        buttonClear = findViewById(R.id.button_clear);
        buttonClose = findViewById(R.id.button_close);
        textViewResult = findViewById(R.id.text_view_result);

        buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateConversion();
            }
        });

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearFields();
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calculateConversion() {
        String input = editTextInput.getText().toString();

        if (input.isEmpty()) {
            Toast.makeText(this, "Por favor ingrese un valor", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            double value = Double.parseDouble(input);
            double result;

            if (radioFahrenheitToCelsius.isChecked()) {
                result = (value - 32) * 5 / 9;
                textViewResult.setText(String.format("%.2f °F = %.2f °C", value, result));
            } else if (radioCelsiusToFahrenheit.isChecked()) {
                result = (value * 9 / 5) + 32;
                textViewResult.setText(String.format("%.2f °C = %.2f °F", value, result));
            }
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Por favor ingrese un número válido", Toast.LENGTH_SHORT).show();
        }
    }

    private void clearFields() {
        editTextInput.setText("");
        textViewResult.setText("");
        radioFahrenheitToCelsius.setChecked(true);
    }
}